interface TopTVEpisodes {
  vfile_id: number;
  media_id: number;
  created_at: string;
  id: number;
  ordering: number;
  description: string;
}

// var myImage = document.querySelector('img');

var requestURL = 'https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json';
var request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.send();

request.onload = function() {
  var superHeroes = request.response;
  console.log(superHeroes)
}

// var myRequest = new Request('./top_tv_episodes.js');

// fetch(myRequest).then(function(response) {
//   return response.json();
// }).then(function(response) {
  // var objectURL = URL.createObjectURL(response);
  // myImage.src = objectURL;
//   var url_arr = response.map(x => x.media_id)
// });

// fetch('top_tv_episodes.json')
//   .then((res:any) => res.json())
//   .then((tv_data) => console.log(tv_data[0]))